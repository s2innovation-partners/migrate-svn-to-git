#!/bin/bash

GIT_REPOS_PROVIDED=0
GET_AUTHORS_LIST=1
SVN_TO_GIT=0
PUSH_TO_GIT=0
VERBOSE=0
REPOSITORY_GROUP="tango-controls/device-servers/DeviceClasses"
AUTHORS_DIR='./authors'

AUTHORS_FILE_PATH="$AUTHORS_DIR/authors.txt"
if [[ -f "$FILE" ]]; then
  sudo chmod 777 $AUTHORS_FILE_PATH
fi 


usage() {
  cat << EOF 
Usage: bash migratesh  -s svn_repos_file_path [-a authors_file_path] [-h] [-g] [-f]

Script to migrate repositories from SVN to Gitlab using svn2git tool.

Available options:

-h, --help             Print this help and exit
-s, --svnrepospath     Path to file with SVN repositories list
-a, --authorspath      Path to file with commits authors 
-g, --gitreposprovided List of Gitlab repositories provided after whitespace in SVN repository list
-p, --pushtogit        Push repository to Gitlab
-v, --verbose          Show more info during script run
EOF
  exit
}

while [[ $# -gt 0 ]]; do
  case $1 in
    -s|--svnrepospath)
      SVN_REPOS_FILE_PATH="$2"
      shift # past argument
      shift # past value
      ;;
    -a|--authorspath)
      AUTHORS_FILE_PATH="$2"
      GET_AUTHORS_LIST=0
      SVN_TO_GIT=1
      shift # past argument
      shift # past value
      ;;
    -g|--gitreposprovided)
      GIT_REPOS_PROVIDED=1
      shift
      ;;
    -p|--pushtogit)
      PUSH_TO_GIT=1
      shift
      ;;
    -v|--verbose)
      VERBOSE=1 
      shift
      ;;
    -h| --help)
      usage
      echo "USAGE:  bash migrate-repo.sh -s"
      
  esac
done

if [[ VERBOSE -eq 1 ]];
  then
    echo "SVN REPOS FIL EPATH  = ${SVN_REPOS_FILE_PATH}"
    echo "AUTHORS FILE PATH     = ${AUTHORS_FILE_PATH}"
    echo "GIT REPOS PROVIDED         = ${GIT_REPOS_PROVIDED}"
fi


cat $SVN_REPOS_FILE_PATH |
while read line
do 
  echo "+++++++++++++++"

  if [[ $line = \#* ]]; # if line starts with # go to next line
    then continue
  fi

if [[ GIT_REPOS_PROVIDED -eq 1 ]];
    then
    read -a strarr <<< $line
    SVN_REPO=${strarr[0]} 
    GIT_REPO=${strarr[1]}
    REPO=$(echo "$GIT_REPO" | awk -Fgitlab.com/ '{print $(NF)}')
    SSH_REPO=git@gitlab.com:$REPO.git
else 
    SVN_REPO=$line
    GIT_REPO=$(echo "$SVN_REPO" | awk -FDeviceClasses/ '{print $(NF)}')
    SSH_REPO=git@gitlab.com:$REPOSITORY_GROUP/$GIT_REPO.git
fi

echo $SVN_REPO
echo $SSH_REPO
DS_NAME=$(echo "$GIT_REPO" | awk -F/ '{print $(NF)}')

# if AUTHORS_FILE_PATH path not passed (argument -a), build file with all SVN repositories commits authors
if [[ GET_AUTHORS_LIST -eq 1 ]]; 
then
  DS_NAME=$(echo "$GIT_REPO" | awk -F/ '{print $(NF)}')
  echo $DS_NAME

  svn log --quiet "$SVN_REPO" | grep -E "r[0-9]+ \| .+ \|" | cut -d'|' -f2 | sed 's/ //g' | sort | uniq >> $AUTHORS_FILE_PATH
  sort -u -o $AUTHORS_FILE_PATH $AUTHORS_FILE_PATH
  continue
fi


if [[ SVN_TO_GIT -eq 1 ]];
    then
  mkdir -p $DS_NAME
  cd $DS_NAME

  if [[ "$AUTHORS_FILE_PATH" = /* ]]; then
    svn2git $SVN_REPO --authors  ".$AUTHORS_FILE_PATH"
  else
    svn2git $SVN_REPO --authors  "../$AUTHORS_FILE_PATH"
  fi

  # push to git
  if [[ PUSH_TO_GIT -eq 1 ]];
  then
    git remote add origin $SSH_REPO
    git push --all origin
    git push --tags origin
  fi 

  cd ../
  sudo rm -rf $DS_NAME
fi

done

if [[ GET_AUTHORS_LIST -eq 1 ]]; 
    then
    echo ""
    echo "Authors file updated. Path: $AUTHORS_FILE_PATH. Please add mapping in file for each username to as follows"
    echo "jbrown = John Brown <johnbrown@example.com>"
fi
