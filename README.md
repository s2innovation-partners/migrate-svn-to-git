# Migrate Svn To Git

Repository with scripts to move Tango Device Classes repostiories from SVN to GitLab and close SVN repository. 
Check [documentation](./How_to_migrate_the_repository_from_SVN_to_Gitlab.pdf) describing how to use scripts.
