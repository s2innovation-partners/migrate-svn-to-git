#!/bin/sh

if [ $# -ne 2 ] ; then
	echo "Usage: $0 <svn url without trunk> <git url>" >&2
	exit 1
fi

_svn=$1
_git=$2

if [ -d "trunk" ] ; then
	echo "trunk: already exists"
	exit 1
fi

svn ls ${_svn}/trunk/ --depth empty
if [ $? -ne 0 ]; then
	echo "Unable to access to ${_svn}" >&2
	exit 1
fi


svn copy ${_svn}/trunk/ ${_svn}/tags/latest -m "Tag of latest version before migration"
if [ $? -ne 0 ]; then
	echo "An error occurred when creating tag of latest version before migration" >&2
	exit 1
fi

svn co ${_svn}/trunk/ trunk
if [ $? -ne 0 ]; then
	echo "Unable to checkout ${_svn}/trunk/" >&2
	exit 1
fi

cd trunk
svn rm *
echo "Moved to ${_git}" > MOVETO
svn add MOVETO
svn commit -m "Redirecting user to gitlab repository ${_git}"
if [ $? -ne 0 ]; then
	echo "Unable to commit MOVETO file" >&2
	exit 1
fi

cd ..
rm -rf trunk

